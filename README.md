# demo-gitlab
## Merge Request
- Merge Request Templates


## Issue、Issue Board 和 Kanban
- https://ithelp.ithome.com.tw/articles/10217957
## gitlab runner
- 資料來源: https://ithelp.ithome.com.tw/articles/10218938
- 通常自架完server ,還需要準備runner
- ![image.png](./image.png)

## pipeline 語法
- 觸發條件
  - 語法
    - only / except
        - 簡單
    - rules
        - 彈性
        - `- if: '$CI_PIPELINE_SOURCE == "merge_request_event"'`
  -  值
    - branch name or tag name
      - 支持簡單正規表達式
    - evnet
      - https://docs.gitlab.com/ee/ci/jobs/job_control.html
      - schedule
      - web (透過gitlab web觸發)
      - trigger (用trigger token觸發)
      - merge_request_event (申請mr時才觸發)
      - changes (source code 路徑底下有改才會觸發)

## template

## anchor

## multi pipeline

## 動態產生pipeline

## Test coverage (report 功能)

## 看ithome 沒用過的
- ansible-playbook 
- environment (沒甚麼太多場景用到)
- Issue templates
- Auto DevOps
  - argo 取代自動deploy
  - Auto Browser Performance Testing (感覺可以試試)
    - https://ithelp.ithome.com.tw/articles/10226588
  - Auto Monitoring
    - https://ithelp.ithome.com.tw/articles/10226931
 - Cycle Analytics
   - https://ithelp.ithome.com.tw/articles/10227541
    

